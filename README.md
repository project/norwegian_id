_CONTENTS OF THIS FILE_

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

# Introduction

The Norwegian Persona ID module defines a field type that allows submitting
only "National ID number", also known as *Fødselsnummer*.
There is a complete validation based on the official information from the
national tax office[1] *(aka Skatteetaten)* and from wikipedia[2]. 

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/2859375

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2859375

- [1] https://www.skatteetaten.no/en/person/National-Registry/Birth-and-name-selection/Children-born-in-Norway/National-ID-number/
- [2] https://no.wikipedia.org/wiki/F%C3%B8dselsnummer


# Requirements

This module does not depend on any contrib module, the only requirement is to
enable the core "string" module. 


# Installation

Install as you would normally install a contributed Drupal module. See
[the online documentation](https://www.drupal.org/docs/8/extending-drupal-8/installing-modules) 
for further information.


# Configuration

No configuration needed. Simply add a field to the entity as you normally do.
[More info here.](https://www.drupal.org/docs/7/nodes-content-types-and-fields/working-with-content-types-and-fields-drupal-7-and-later)


# Maintainers

Current maintainer:
 * Marlon (esolitos) - https://www.drupal.org/user/1567500
